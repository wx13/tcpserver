Cursor-on-Target
================

This repository contains both server and client code for CoT messaging.


CoT Server
----------

Our CoT server is really just a tcp server.

### Usage

	$ ruby cot_server.rb -h
	Usage: cot_server [options]
	    -p, --port PORT                  Specify TCP port
	    -a, --address IP                 Specify TCP address
	    -d, --debug                      Turn on debugging

To have the server accept any ip address and run on port 8080, run:

	$ ruby tcp_server.rb -a 0.0.0.0 -p 8080


### Deploy

It is recommended to use a process manager to keep the server running.
Install supervisor:

	$ sudo aptitude install supervisor

Then place the following file in
`/etc/supervisor/conf.d/tcp_server.conf`

	[program: cot_server]
	command = ruby /.../.../cot_server.rb -p 8080 -a 0.0.0.0
	directory = /.../.../
	autostart = true
	autorestart = true
	startretries = 3
	stderr_logfile = /.../.../cot_server.error
	stdout_logfile = /.../.../cot_server.log
	user = ...
	environment=USER=...,HOME=...

and run `supervisorctl start cot_server`.


### Deploy with ansible

	ansible-playbook cot_server.yaml -i hosts --ask-pass --ask-sudo-pass


CoT client
----------

The CoT client code can be used against our CoT server or any other CoT server.
It can be used as a library for other python code, or run on the command line.
For example, to send wind speed and direction:

	python cot_client.py -i localhost -p 8080 -w 3.2 187.1

To monitor the CoT server (listen and print) run:

	python cot_client.py -i localhost -p 8080 -l


Send MET data
-------------

To send MET data on the command line, first you must create a base MET file.
The file `met_data.yaml` shows an example.  It looks like this:

	lat: 34.239953
	lon: -118.563773
	id: MET_001
	MET:
	  wind:
	    speed:
	      '@units': m/s
	    dir:
	      '@units': degrees CCW from east
	  pressure:
	    '@units': mb
	  temperature:
	    '@units': C
	  humidity:

This file sets geolocation, unit id, and units for measurements. Running:

	python cot_client.py -i localhost -p 8080 -m met_data.yaml

will send an empty met message (no data).  To send data, use one or more of
the following flags:

	-T, --temperature <temperature>
	-H, --humidity <humidity>
	-P, --pressure <pressure>
	-W, --wind <speed> <direction>

