import time
import socket
import xmltodict
import argparse
from jinja2 import Template
from datetime import datetime, timedelta
import xmltodict
import numpy as np
import re
import yaml



class CoT:

	def __init__(self,ip,port):
		self.ip = ip
		self.port = port
		self.open()
		self.template = (
			"<?xml version='1.0' standalone='yes'?>"
			"<event how='m-g' stale='{{ stale }}' start='{{ start }}' time='{{ time }}' type='a-f-G-E-S' uid='{{uid}}' version='2.0'>"
				"<point lat='{{ lat }}' lon='{{lon}}' hae='100.0' ce='0.0' le='0.0' />"
				"<detail>{{ detail }}</detail>"
			"</event>"
			)
		self.template = Template(self.template)

	def open(self):
		self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
		self.sock.connect((self.ip,self.port))
		self.rf = self.sock.makefile('rb')
		self.wf = self.sock.makefile('wb')

	def write(self,msg):
		self.wf.write(str(msg)+"\n")

	def read(self):
		return self.rf.read()

	def listen(self,pattern='{}'):
		try:
			pattern = yaml.load(pattern)
		except:
			print "invalid pattern:", pattern
			return
		while True:
			for xml in self.rf:
				data = self.parse_xml(xml)
				if not data:
					continue
				if 'uid' in pattern and not re.match(pattern['uid'],data['event']['@uid']):
					print 'Does not match UID'
					continue
				if 'detail' in pattern and pattern['detail'] not in data['event']['detail']:
					print 'Does not match detail'
					continue
				print xml
			time.sleep(2)
			print 'Connection closed. Reopening'
			self.open()

	def close(self):
		self.sock.close()

	def parse_xml(self,xml):
		try:
			data = xmltodict.parse(xml)
		except:
			try:
				data = xmltodict.parse('<?xml version="1.0" encoding="UTF-8"?>'+xml.replace('<?xml version="1.0" encoding="UTF-8"?>',''))
			except:
				print "Error: invalid XML"
				return None
		return data

	def anomaly(self):
		for xml in self.rf:
			data = self.parse_xml(xml)
			if not data:
				continue
			if 'event' in data and 'detail' in data['event'] and 'TRAP' in data['event']['detail']:
				data['event']['detail'] = {
					'TRAP_anomaly': {
						'anomaly_value': np.random.normal(),
						'anomaly_threshold': np.random.normal(loc=3.5,scale=0.1)
					}
				}
				xml = xmltodict.unparse(data).replace('\n','')
				print xml
				self.write(xml)
				break

	def times(self,time):
		return {
			'time': datetime.strftime(time,'%Y-%m-%dT%H:%M:%SZ'),
			'start': datetime.strftime(time,'%Y-%m-%dT%H:%M:%SZ'),
			'stale': datetime.strftime(time+timedelta(days=1),'%Y-%m-%dT%H:%M:%SZ')
		}

	def met(self,met_data,args):
		data = self.times(datetime.now())
		data['lat'], data['lon'], data['uid'] = met_data['lat'], met_data['lon'], met_data['id']
		del met_data['lat']
		del met_data['lon']
		del met_data['id']
		if args.wind:
			met_data['MET']['wind']['speed']['#text'] = str(args.wind[0])
			met_data['MET']['wind']['dir']['#text'] = str(args.wind[1])
		if args.humidity:
			met_data['MET']['humidity'] = {'#text':str(args.humidity)}
		if args.temperature:
			met_data['MET']['temperature']['#text'] = str(args.temperature)
		if args.pressure:
			met_data['MET']['pressure']['#text'] = str(args.pressure)
		detail = xmltodict.unparse(met_data,full_document=False)
		data['detail'] = detail
		self.write(self.template.render(data))

	def trap(self):
		data = self.times(datetime.now())
		data['detail'] = Template(
			"<TRAP>"
				"<counts>{{ counts | default(0) }}</counts>"
				"<dot_output>{{ dot_output | default('0,0,0,0,0,0,0,0,0,0') }}</dot_output>"
				"<dot_threshold>{{ dot_threshold | default('0,0,0,0,0,0,0,0,0,0') }}</dot_threshold>"
				"<norm1>{{ 'norm1[0],norm1[1]' if norm else '5.0,10000.0' }}</norm1>"
				"<hist1>'{{ 'hist1' | default('(1176)') }}</hist1>"
			"</TRAP>"
		).render()
		self.write(self.template.render(data))


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--ip',       default='localhost', action='store',      help='Specify server ip to connect to. [localhost]')
	parser.add_argument('-p', '--port',     default=8080,        action='store',      help='Specify server port to connect to. [8080]', type=int)
	parser.add_argument('-s', '--send',     default=False,       action='store',      help='Specify a string to send as a message')
	parser.add_argument('-l', '--listen',   default=False,       action='store_true', help='Listen for messages and print')
	parser.add_argument('-f', '--filter',   default=False,       action='store',      help='Just like listen, but filter results')
	parser.add_argument('-t', '--trap',     default=False,       action='store_true', help='Send a TRAP message')
	parser.add_argument('-m', '--met',      default=False,       action='store',      help='Send a MET message')
	parser.add_argument('-W', '--wind',     default=False,       action='store',      help='Specify wind speed/direction', nargs=2, type=float)
	parser.add_argument('-P', '--pressure', default=False,       action='store',      help='Specify barometric pressure', type=float)
	parser.add_argument('-H', '--humidity', default=False,       action='store',      help='Specify relative humidity', type=float)
	parser.add_argument('-T', '--temperature',default=False,     action='store',      help='Specify temperature', type=float)
	parser.add_argument('-A', '--anomaly',  default=False,       action='store_true', help='Send fake anomaly messages')
	args = parser.parse_args()

	cot = CoT(args.ip,args.port)

	if args.send:
		cot.write(args.send)
		cot.close()

	if args.listen:
		cot.listen()
		cot.close()

	if args.filter:
		cot.listen(args.filter)
		cot.close()

	if args.met:
		import yaml
		data = yaml.load(open(args.met))
		cot.met(data,args)
		cot.close()

	if args.trap:
		cot.trap()
		cot.close()

	if args.anomaly:
		while True:
			cot.anomaly()
			cot.close()
			time.sleep(2)
			cot.open()
		cot.close()
