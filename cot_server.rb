require 'socket'
require 'optparse'
require 'thread'

$stdout.sync = true

$mutex = Mutex.new

def puts(str)
	$stdout.print str + "\n"
end

class Server

	def initialize(ip,port,mc)
		@server = TCPServer.new(ip,port)
		@clients = {}
		@max_conn = mc
	end

	def send_to_connection(msg,name,connection)
		begin
			puts "Sending to #{connection.inspect} #{connection.peeraddr(:hostname)}"
			connection.puts msg
		rescue
			puts "Could not send to #{connection.inspect}"
			close_connection(connection,name)
		end
	end

	def close_connection(connection,client_name)
		begin
			puts "Closing connection #{connection.inspect}"
			connection.close
		rescue
			puts "*** Error: could not close connection " + connection.inspect
		end
		begin
			puts "Removing closed connection #{connection.inspect}"
			$mutex.synchronize{@clients[client_name].delete(connection)}
		rescue
			puts "*** Error: could not remove connection from list " + connection.inspect
		end
		if @clients[client_name].length == 0
			@clients.delete(client_name)
		end
	end

	def send_to_all(msg,connection)
		puts "Sending data to #{@clients.length} clients"
		@clients.each do |other_name,other_client|
			puts "    Sending to #{other_client.length} connections"
			puts other_client.inspect
			other_client.each do |other_connection|
				if other_connection != connection
					send_to_connection(msg, other_name, other_connection)
				end
			end
		end
	end

	def accept_connection(connection)
		name = connection.peeraddr(:hostname)[2,3].join(':')
		$mutex.synchronize{
			if @clients.has_key?(name)
				@clients[name] << connection
			else
				@clients[name] = [connection]
			end
			puts "Accepted connection from #{connection.peeraddr(:hostname)} #{connection.inspect}"
			ex_clients = @clients[name].slice!(0..-(@max_conn+1))
			begin
				ex_clients.each{|c|
					puts "Closing excess connection #{c.inspect}"
					c.close
				}
			rescue
				puts "*** Error: Trouble deleting execess connections"
			end
		}
		puts "#{@clients[name].length} client connections (#{@clients.length} total hosts)"
	end

	def start
		puts "Starting TCP server"
		loop do
			Thread.start(@server.accept) do |connection|
				accept_connection(connection)
				loop do
					msg = connection.gets.chomp
					puts "Received message from #{connection.peeraddr(:hostname)}"
					puts msg
					send_to_all(msg,connection)
				end
			end
		end.join
	end

end


if __FILE__ == $0

	options = {:max_conn=>10}
	OptionParser.new{|opts|
		opts.on("-p","--port PORT","Specify TCP port"){|port|
			options[:port] = port
		}
		opts.on("-a","--address IP","Specify TCP address"){|ip|
			options[:ip] = ip
		}
		opts.on("-d","--debug","Turn on debugging"){
			options[:debug] = true
		}
		opts.on("-m","--max-connections N","Maximum number of connections per ip/hostname [10]"){|max|
			options[:max_conn] = max.to_i
		}
	}.parse!

	if options[:port].nil? or options[:ip].nil?
		puts "Must specify IP and port"
		exit
	end

	if options[:debug]
		$stdout.sync = true
	end

	server = Server.new(options[:ip],options[:port],options[:max_conn])
	server.start

end
