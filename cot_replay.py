import time
import socket
import xmltodict
import argparse
from datetime import datetime, timedelta



class CoT:

	def __init__(self,args):
		self.ip = args.ip
		self.port = args.port
		self.speed = args.speed
		self.sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)


	# Loop over all lines of all specified files and:
	#  - parse the XML data to get the date-time
	#  - subtract the last time, and wait this many seconds
	#  - send the data.
	def replay(self,files):
		t0 = None
		for file in files:
			with open(file,'r') as f:
				for line in f:
					data = self.parse_xml(line)
					if not data:
						continue
					t = datetime.strptime(data['event']['@time'],'%Y-%m-%dT%H:%M:%SZ')
					if t0:
						if t < t0:
							continue
						time.sleep((t-t0).total_seconds()/self.speed)
					t0 = t
					self.send(line)

	# Parse the cursor-on-target XML data.
	# Some CoT servers put the XML header at the end of the message.
	# This results in a parse failure.  We catch this failure, and try
	# putting an XML header at the start.
	@staticmethod
	def parse_xml(xml):
		try:
			data = xmltodict.parse(xml)
		except:
			try:
				data = xmltodict.parse('<?xml version="1.0" encoding="UTF-8"?>'+xml.replace('<?xml version="1.0" encoding="UTF-8"?>',''))
			except:
				print "Error: invalid XML"
				return None
		return data

	# Just send a string over UDP
	def send(self,line):
		self.sock.sendto(line,(self.ip,self.port))

	# Listen for UDP messages and print to screen.
	def listen(self):
		self.sock.bind((self.ip,self.port))
		while True:
			data = self.sock.recv(65536)
			print data


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('-i', '--ip',       default='localhost', action='store',      help='Specify server ip to connect to. [localhost]')
	parser.add_argument('-p', '--port',     default=8088,        action='store',      help='Specify server port to connect to. [8088]', type=int)
	parser.add_argument('-s', '--speed',    default=1.0,         action='store',      help='Set playback speed [1.0 (real-time)]', type=float)
	parser.add_argument(      'files',      default=None,        action='store',      help='Specify files for playback', nargs='*')
	parser.add_argument('-l', '--listen',   default=False,       action='store_true', help='Listen for messages (for testing) [False]')
	args = parser.parse_args()

	cot = CoT(args)

	if args.listen:
		cot.listen()
	else:
		cot.replay(args.files)

